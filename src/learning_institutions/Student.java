package learning_institutions;
import java.util.Random;

public class Student 
{
	private String name;
	private int age;
	private String gender;
	private double avgGrade;
	private int entityId;
	private LearningInstitution studyPlace;
	
	public Student(int age, String gender) 
	{
		this.name = NameGenerator.generate(gender);
		setAge(age);
		setGender(gender);
		setGrade();
	}
	
	public void setGender(String gender) {
		if (!gender.equals("male") && !gender.equals("female"))
		{
			throw new IllegalArgumentException("Gender must be 'male' or 'female'.");
		}
		
		this.gender = gender;
	}
	
	public void setAge(int age) {
		if (age < 7) 
		{
			throw new IllegalArgumentException("Student must be older than 6.");
		}
		this.age = age;
	}
	
	private void setGrade() 
	{
		Random r = new Random();
		double random = 2 + (6 - 2) * r.nextDouble();
		
		double finGrade = Math.round(random * 100.0) / 100.0;
		this.avgGrade = finGrade;
	}
	
	
	protected void setId(int id) 
	{
		this.entityId = id;
	}
	
	
	// used instead of the id 
	
	protected void setStudyPlace(LearningInstitution institution) {
		this.studyPlace = institution;
	}
	
	protected String getGender()
	{
		return this.gender;
	}
	
	protected String getName()
	{
		return this.name;
	}
	
	protected double getAvgGrade()
	{
		return this.avgGrade;
	}
	
	public double calculatePayment()
	{
		return (this.age / this.studyPlace.getAvgGrade()) * 100.0 + this.studyPlace.getTax();
	}

	
	 @Override
	    public String toString() 
	 { 
        return String.format("Student: %s, Avg.grade: %.2f, School/Uni: %s, Payment: $%.2f",
        		this.getName(), this.avgGrade, this.studyPlace.getName(), this.calculatePayment());
	 }
	
}
	