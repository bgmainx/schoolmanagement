package learning_institutions;
import java.util.Random;

public abstract class NameGenerator {
	private static final String[] FEMALE_NAMES = new String[] 
			{
					"Mariq", "Darina", "Pavlina", "Ganka", "Ivanka", "Stefanka", "Dochka",
					"Tochka", "Stefaniq", "Korneliq"
			};
	
	private static final String[] MALE_NAMES = new String[]
			{
					"Vasko", "Petyr", "Stefan", "Georgi", "Kaloqn","Ivan","Dinko",
					"Stef", "Staiko", "Joro", "Kosta",
					"Vtori Ivan", "Dian", "Petrohan"
			};
	
	private NameGenerator(){}
	
	
	public static String generate(String gender)
	{
		int randomIndex = -1;
		Random rand = new Random();
		
		if (gender.equals("male")) 
		{
			randomIndex = rand.nextInt(((MALE_NAMES.length - 1 - 0) + 1) + 0);
			
			return MALE_NAMES[randomIndex];
		} 
		else if (gender.equals("female"))
		{
			randomIndex = rand.nextInt(((FEMALE_NAMES.length - 1 - 0) + 1) + 0);
			
			return FEMALE_NAMES[randomIndex];
		} 
		else
		{
			throw new IllegalArgumentException("Invalid gender.");
		}
	}
}
