package learning_institutions;
import java.util.Random;

public class Main 
{

	public static void main(String[] args) 
	{
		School school1 = new School("Geo Milev", "Varna");
		School school2 = new School("A.S. Pushkin", "Varna");
		School school3 = new School("4ta Gimnaziq", "Varna");

		University university1 = new University("TU", "Varna");
		University university2 = new University("VINS", "Varna");
		
		LearningInstitution[] institutions = new LearningInstitution[]
				{
						school1, school2, school3, university1, university2
				};


		int age;
		String gender;
		Random randomAge = new Random();
		
		for (int i = 1; i <= 10; i++) 
		{
			age = randomAge.nextInt(100 - 7 + 1) + 7;
			
			if (i % 2 == 0) 
			{
				gender = "male";
			} 
			else 
			{
				gender = "female";
			}
			
	
			// The NameGenerator generates random name in the Students constructor.
			
			school1.addStudent(new Student(age, gender));
			school2.addStudent(new Student(age, gender));
			school3.addStudent(new Student(age, gender));
			
			university1.addStudent(new Student(age, gender));
			university2.addStudent(new Student(age, gender));
		}
		
		System.out.println("$" + school1.getTotalIncome());
		System.out.println("$" + school2.getTotalIncome());
		System.out.println("$" + university1.getTotalIncome());

		for (LearningInstitution learningInstitution : institutions) 
		{
			System.out.println(learningInstitution.getTopPerformerByGender("male"));
			System.out.println(learningInstitution.getTopPerformerByGender("female"));
		}
		
		System.out.println();
		System.out.println("Top contributor is - " + getTopContributorForAllInst(institutions));
		
		
	}
	
	public static Student getTopContributorForAllInst(LearningInstitution[] institutions) 
	{
		double max = Integer.MIN_VALUE;
		Student topContributor = null;
		
		for (LearningInstitution inst : institutions) 
		{
			if (inst.getTopContributor().calculatePayment() > max) 
			{
				topContributor = inst.getTopContributor();
				max = inst.getTopContributor().calculatePayment();
			}
		}
		
		return topContributor;
	}
}
