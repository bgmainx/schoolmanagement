package learning_institutions;
public class School extends LearningInstitution
{
	private static final double tax = 50;
	
	public School(String name, String address) 
	{
		super(name, address);
	}
	
	@Override
	protected double getTax() {
		return School.tax;
	}
}

