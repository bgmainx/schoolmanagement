package learning_institutions;
import java.util.ArrayList;
import java.util.Comparator;

public abstract class LearningInstitution {
	private static int id;
	private String name;
	private String address;
	private ArrayList<Student> students;
	
	public LearningInstitution(String name, String address) 
	{
		LearningInstitution.id++;
		this.setName(name);
		this.setAddress(address);
		students = new ArrayList<>();
	}
	
	public void setName(String name) 
	{
		if (name.equals("")) 
		{
			throw new IllegalArgumentException("Invalid name!");
		}
		this.name = name;
	}
	
	public void setAddress(String address) 
	{
		if (address.equals("")) 
		{
			throw new IllegalArgumentException("Invalid Address!");
		}
		
		this.address = address;
	}

	
	// Adds Student to the list and sets itself as a learning institution of the student.
	public void addStudent(Student student) 
	{
		if (student == null) 
		{
			throw new IllegalArgumentException("Invalid student!");
		}
		
		student.setId(LearningInstitution.id);
		
		student.setStudyPlace(this);
		this.students.add(student);
		this.showWelcomeMsg();
	}

	private void showWelcomeMsg() 
	{
		System.out.printf("Welcome %s and welcome to %s %n",
				students.get(students.size() - 1).getName(),
				this.getName());
	}

	protected String getName() 
	{
		return this.name;
	}

	public double getAvgGrade() 
	{
		double sum = 0;
		for (int i = 0; i < this.students.size(); i++) 
		{
			sum += students.get(i).getAvgGrade();
		}
		
		double result = sum / this.students.size();

		return Math.round(result * 100.0) / 100.0;
	}

	public Student getTopPerformer() 
	{
		students.sort(Comparator.comparing(Student::getAvgGrade));
		return students.get(this.students.size() - 1);
	}
	
	
	public Student getTopPerformerByGender(String gender) 
	{
		ArrayList<Student> specificGenderStudents = new ArrayList<Student>();
		
		this.students.forEach(s -> 
		{
			if (s.getGender().equals(gender)) {
				specificGenderStudents.add(s);
			}
		});
		
		specificGenderStudents.sort(Comparator.comparing(Student::getAvgGrade));
		return specificGenderStudents.get(specificGenderStudents.size() - 1);
	}
	
	
	
	public double getTotalIncome() 
	{
		double totalIncome = 0;
	
		for (Student student : students) {
			totalIncome += student.calculatePayment();
		}
		
		return Math.round(totalIncome * 100.0) / 100.0;
	}
	
	
	public Student getTopContributor() 
	{
		double max = Integer.MIN_VALUE;
		Student topContributor = null;
		
		for (Student student : students) 
		{
			if (student.calculatePayment() > max) 
			{
				topContributor = student;
				max = student.calculatePayment();
			}
		}
		
		return topContributor;
	}
	
	// Note to ask Evgeni about something.
	protected abstract double getTax();

}
