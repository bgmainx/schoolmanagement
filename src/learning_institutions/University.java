package learning_institutions;

public class University extends LearningInstitution 
{
	private static final double tax = 100;

	public University(String name, String address) 
	{
		super(name, address);
	}
	
	@Override
	protected double getTax() {
		return University.tax;
	}
}
